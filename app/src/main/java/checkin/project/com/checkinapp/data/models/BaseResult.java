package checkin.project.com.checkinapp.data.models;

/**
 * Created by khacvy on 4/10/17.
 */

public class BaseResult {
    private String errors;

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }
}
