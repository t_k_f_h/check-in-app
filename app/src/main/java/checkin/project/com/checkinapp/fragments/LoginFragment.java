package checkin.project.com.checkinapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import checkin.project.com.checkinapp.LoginActivity;
import checkin.project.com.checkinapp.R;
import checkin.project.com.checkinapp.data.AppApi;
import checkin.project.com.checkinapp.data.PreferenceManager;
import checkin.project.com.checkinapp.data.callbacks.LoginCallback;
import checkin.project.com.checkinapp.data.models.LoginResponse;
import checkin.project.com.checkinapp.ui.LoadingUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private EditText txtEmail;
    private EditText txtPassword;
    private CheckBox cbRememberMe;
    private Button btnLogin;

    private AppApi mAppApi;
    private PreferenceManager mPreferenceManager;

    public LoginFragment() {
        mAppApi = new AppApi();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mPreferenceManager = new PreferenceManager(getActivity());
        initUI(view);
        return view;
    }

    private void initUI(View view) {
        txtEmail = (EditText) view.findViewById(R.id.txt_email);
        txtPassword = (EditText) view.findViewById(R.id.txt_password);
        btnLogin = (Button) view.findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processLogin();
            }
        });
    }

    private void processLogin() {
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        if(validate(email, password)){
            LoadingUtil.showLoading(getActivity());
            mAppApi.authenticate(email, password, new LoginCallback() {
                @Override
                public void onSuccess(LoginResponse s) {
                    LoadingUtil.hideLoading();
                    mPreferenceManager.saveToken(s.getToken());
                    ((LoginActivity) getActivity()).goToMainScreen();
                }

                @Override
                public void onError(String error) {
                    LoadingUtil.hideLoading();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onLoginFailed() {
                    LoadingUtil.hideLoading();
                    Toast.makeText(getActivity(), "These credentials do not match our records.", Toast
                            .LENGTH_LONG).show();
                }
            });
        }
    }

    private boolean validate(String email, String password){
        if(TextUtils.isEmpty(email)){
            txtEmail.setError("The email field is required.");
            txtEmail.requestFocus();
            return false;
        }
//        else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
//            txtEmail.setError("The email field is not valid.");
//            txtEmail.requestFocus();
//            return false;
//        }
        else if(TextUtils.isEmpty(password)){
            txtPassword.setError("The password field is required.");
            txtPassword.requestFocus();
            return false;
        }

        return true;
    }


}
