package checkin.project.com.checkinapp.data;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Levi on 12/11/2016.
 */
public class BaseRequest<T> extends Request<T> {
    private static final String TAG = "BaseRequest";

    private Type mType;
    private Map<String, String> headers;
    private JSONObject mRequestBody;
    private Map<String, String> mBody;
    private Response.Listener<T> listener;
    private PreferenceManager mPreferenceManager;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url     URL of the request to make
     * @param type    Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     */
    public BaseRequest(String url, Type type,
                       Map<String, String> headers,
                       Response.Listener<T> listener,
                       Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.mType = type;
        this.headers = headers;
        this.listener = listener;
    }

    public BaseRequest(String url, Type type,
                       Map<String, String> headers,
                       Map<String, String> requestBody,
                       Response.Listener<T> listener,
                       Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        this.mType = type;
        this.headers = headers;
        this.mBody = requestBody;
        this.listener = listener;
    }

    public BaseRequest(Context context,
                       String url,
                       int method,
                       Type type,
                       Map<String, String> requestBody,
                       Map<String, String> headers,
                       Response.Listener<T> responseListener,
                       Response.ErrorListener errorListener) {
        super(method, url, errorListener);

        this.mType = type;
        this.headers = headers;
        this.mBody = requestBody;
        this.listener = responseListener;

        mPreferenceManager = new PreferenceManager(context);
        String token = mPreferenceManager.getToken();
        if (this.headers == null) {
            this.headers = new HashMap<>();
        }
        this.headers.put("Authorization", "Bearer " + token);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded";
    }

    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        RetryPolicy re = new DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        return super.setRetryPolicy(re);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return this.mBody == null ? super.getParams() : mBody;
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            Gson gson = new Gson();
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            Log.d(TAG, "Call API " + getUrl() + " success, data is \n" + json);
            Response res = Response.success(gson.fromJson(json, mType), HttpHeaderParser
                    .parseCacheHeaders(response));

            return res;
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "[UnsupportedEncodingException]" + e);
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            Log.e(TAG, "[UnsupportedEncodingException]" + e);
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        String responseCode = "UNKNOWN";
        String responseBody = "No Body";
        NetworkResponse response = volleyError.networkResponse;
        if (response != null) {
            try {
                 responseBody = new String(response.data, "utf-8");
            } catch (Exception e) {
                responseBody = e.toString();
            }
        }else {
            responseBody = volleyError.getClass().getSimpleName();
        }
        Log.d(TAG, "Call API " + getUrl() + " error. Response Code " + responseCode + "\n Body : " +
                "" + responseBody);
        return super.parseNetworkError(volleyError);
    }
}
