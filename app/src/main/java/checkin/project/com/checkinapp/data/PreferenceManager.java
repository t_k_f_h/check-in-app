package checkin.project.com.checkinapp.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import checkin.project.com.checkinapp.data.models.BusinessInfo;

/**
 * Created by Levi on 12/11/2016.
 */
public class PreferenceManager {
    private static final String APP_PRE = "app-news";
    private static final String KEY_PREF_TOKEN = "user_token";
    private static final String KEY_PREF_BUSINESS_INFO = "business_info";

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    public PreferenceManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(APP_PRE, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public void saveToken(String token) {
        mEditor.putString(KEY_PREF_TOKEN, token).commit();
    }


    public String getToken(){
        return mSharedPreferences.getString(KEY_PREF_TOKEN, null);
    }

    public void saveBusinessInfo(BusinessInfo info) {
        Gson gson = new Gson();
        String json = gson.toJson(info);
        mEditor.putString(KEY_PREF_BUSINESS_INFO, json).commit();
    }


    public BusinessInfo getBusinessInfo(){
        String json = mSharedPreferences.getString(KEY_PREF_BUSINESS_INFO, null);
        if(json != null){
            Gson gson = new Gson();
            return gson.fromJson(json, BusinessInfo.class);
        }
        return null;
    }

}
