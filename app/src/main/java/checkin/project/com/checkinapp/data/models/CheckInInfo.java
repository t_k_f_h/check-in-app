package checkin.project.com.checkinapp.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by khacvy on 4/10/17.
 */

public class CheckInInfo {
    private int id;
    private int status;
    @SerializedName("customer_id")
    private int customerId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
