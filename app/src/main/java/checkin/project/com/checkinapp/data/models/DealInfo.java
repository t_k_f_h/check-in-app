package checkin.project.com.checkinapp.data.models;

/**
 * Created by khacvy on 4/9/17.
 */

public class DealInfo {
    private String type;
    private String content;
    private String url;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
