package checkin.project.com.checkinapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import checkin.project.com.checkinapp.data.AppApi;
import checkin.project.com.checkinapp.data.PreferenceManager;
import checkin.project.com.checkinapp.data.callbacks.BaseCallback;
import checkin.project.com.checkinapp.data.models.BusinessInfo;
import checkin.project.com.checkinapp.data.models.CheckInResponse;
import checkin.project.com.checkinapp.data.models.CustomerInfo;
import checkin.project.com.checkinapp.ui.LoadingUtil;

public class UserInfoActivity extends AppCompatActivity {
    private static final String TAG = "UserInfoActivity";

    private EditText txtFirstName;
    private EditText txtLastName;
    private EditText txtEmail;
    private TextInputLayout tilFirstName;
    private TextInputLayout tilLastName;
    private TextInputLayout tilEmail;
    private Spinner spMonth;
    private Spinner spDate;
    private Button btnDone;
    private ImageView imgLogo;
    private TextView textDescription;
    private TextView textGuide;

    List<Integer> listMonths = new ArrayList<>();
    List<Integer> listDays = new ArrayList<>();

    private AppApi mAppApi;
    private PreferenceManager mPreferenceManager;
    private CustomerInfo mCustomerInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        mAppApi = new AppApi();
        mPreferenceManager = new PreferenceManager(this);

        initUIs();
        initEvents();

        if(getActionBar() != null){
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setDisplayShowHomeEnabled(true);
        }

        setupUI();
        setTitle(mPreferenceManager.getBusinessInfo().getName());
        getUserInfo();
        setupMonthList();
    }

    private void setupDateList() {
        Calendar cal = Calendar.getInstance();
        int month = listMonths.get(spMonth.getSelectedItemPosition());
        cal.set(Calendar.MONTH, month - 1);
        int numDays = cal.getActualMaximum(Calendar.DATE);
        listDays.clear();
        for (int i = 1; i <= numDays; i++) {
            listDays.add(i);
        }
        ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<Integer>(this,
                android.R.layout.simple_spinner_dropdown_item,
                listDays);
        spDate.setAdapter(spinnerArrayAdapter);
    }

    private void getUserInfo() {
        Intent intent = getIntent();
        if (intent != null) {
            mCustomerInfo = (CustomerInfo) intent.getSerializableExtra("CUSTOMER_INFO");
            if (mCustomerInfo != null) {
                if (!TextUtils.isEmpty(mCustomerInfo.getFirstName())) {
                    txtFirstName.setText(mCustomerInfo.getFirstName());
                }

                if (!TextUtils.isEmpty(mCustomerInfo.getLastName())) {
                    txtLastName.setText(mCustomerInfo.getLastName());
                }

                if (!TextUtils.isEmpty(mCustomerInfo.getEmail())) {
                    txtEmail.setText(mCustomerInfo.getEmail());
                }
            }
        }
    }

    private void initUIs() {
        txtFirstName = (EditText) findViewById(R.id.txt_first_name);
        txtLastName = (EditText) findViewById(R.id.txt_last_name);
        txtEmail = (EditText) findViewById(R.id.txt_email);
        tilFirstName = (TextInputLayout) findViewById(R.id.input_layout_firtname);
        tilLastName = (TextInputLayout) findViewById(R.id.input_layout_lastname);
        tilEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        spMonth = (Spinner) findViewById(R.id.sp_month);
        spDate = (Spinner) findViewById(R.id.sp_date);
        btnDone = (Button) findViewById(R.id.btn_done);
        imgLogo = (ImageView) findViewById(R.id.img_logo);
        textDescription = (TextView) findViewById(R.id.text_description);
        textGuide = (TextView) findViewById(R.id.text_guide);
    }

    private void initEvents() {
        txtFirstName.addTextChangedListener(new CustomTextWatcher(txtFirstName));
        txtLastName.addTextChangedListener(new CustomTextWatcher(txtLastName));
        txtEmail.addTextChangedListener(new CustomTextWatcher(txtEmail));

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processSubmit();
            }
        });

        spMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setupDateList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupUI() {
        BusinessInfo info = mPreferenceManager.getBusinessInfo();
        if (info != null) {
            setTitle(info.getName());
            textGuide.setText(getString(R.string.input_info_description, info.getName()));
            textDescription.setText(info.getDescription());

            Glide.with(this).load(info.getLogo())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgLogo);
        }
    }

    private void setupMonthList() {
        for (int i = 1; i <= 12; i++) {
            listMonths.add(i);
        }
        ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<Integer>(this,
                android.R.layout.simple_spinner_dropdown_item,
                listMonths);
        spMonth.setAdapter(spinnerArrayAdapter);
    }

    private void processSubmit() {
        String firstName = txtFirstName.getText().toString().trim();
        String lastName = txtLastName.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();

        if (validateFirstName(firstName) && validateLastName(lastName)
                && validateEmail(email)) {
            if (mCustomerInfo == null) {
                mCustomerInfo = new CustomerInfo();
                mCustomerInfo.setPhone("0000000000");
            }
            mCustomerInfo.setFirstName(firstName);
            mCustomerInfo.setLastName(lastName);
            mCustomerInfo.setEmail(email);
            mCustomerInfo.setBirthday(getBirthDay());

            LoadingUtil.showLoading(this);
            mAppApi.updateCustomerInfo(this, mCustomerInfo, new BaseCallback<CustomerInfo>() {
                @Override
                public void onSuccess(CustomerInfo customerInfo) {
                    submitCheckIn(customerInfo.getPhone());
                }

                @Override
                public void onError(String error) {
                    LoadingUtil.hideLoading();
                    String errorMsg = (error != null) ? error : getString(R.string.common_server_error);
                    Toast.makeText(getBaseContext(), errorMsg, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private boolean validateFirstName(String firstName) {
        if (firstName.length() > 0 && (firstName.length() < 2 || firstName.length() > 30)) {
            tilFirstName.setError("Please enter your first name between 2 and 30 characters long.");
            txtFirstName.requestFocus();
            return false;
        } else {
            tilFirstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLastName(String lastName) {
        if (lastName.length() > 0 && (lastName.length() < 2 || lastName.length() > 30)) {
            tilLastName.setError("Please enter your last name between 2 and 30 characters long.");
            txtLastName.requestFocus();
            return false;
        } else {
            tilLastName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail(String email) {
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            tilEmail.setError("Please enter a valid email address.");
            txtEmail.requestFocus();
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }

        return true;
    }

    private void submitCheckIn(String phone) {
        mAppApi.checkInSubmit(this, phone, new BaseCallback<CheckInResponse>() {
            @Override
            public void onSuccess(CheckInResponse response) {
                LoadingUtil.hideLoading();

                Intent intent = new Intent(UserInfoActivity.this, ThanksActivity.class);
                intent.putExtra("customer_subscription_type", 2);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }

            @Override
            public void onError(String error) {
                LoadingUtil.hideLoading();
            }
        });
    }

    private String getBirthDay() {
        int day = listDays.get(spDate.getSelectedItemPosition());
        int month = listMonths.get(spMonth.getSelectedItemPosition());

        return String.format("%d-%d-1900", day, month);
    }

    private class CustomTextWatcher implements TextWatcher {

        private View view;

        private CustomTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString().trim();
            switch (view.getId()) {
                case R.id.txt_first_name:
                    validateFirstName(text);
                    break;
                case R.id.txt_last_name:
                    validateLastName(text);
                    break;
                case R.id.txt_email:
                    validateEmail(text);
                    break;
            }
        }
    }
}
