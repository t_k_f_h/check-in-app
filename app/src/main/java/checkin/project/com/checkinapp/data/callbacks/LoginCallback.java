package checkin.project.com.checkinapp.data.callbacks;

import checkin.project.com.checkinapp.data.models.LoginResponse;

/**
 * Created by khacvy on 4/9/17.
 */

public interface LoginCallback extends BaseCallback<LoginResponse> {
    void onLoginFailed();
}
