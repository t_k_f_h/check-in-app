package checkin.project.com.checkinapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import checkin.project.com.checkinapp.adapters.ViewPagerAdapter;
import checkin.project.com.checkinapp.data.PreferenceManager;
import checkin.project.com.checkinapp.fragments.LoginFragment;
import checkin.project.com.checkinapp.fragments.ScanQRFragment;

public class LoginActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private PreferenceManager mPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPreferenceManager = new PreferenceManager(this);

        if(mPreferenceManager.getToken() != null){
            goToMainScreen();
        }

        setContentView(R.layout.activity_login);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginFragment(), "LOGIN");
        adapter.addFragment(new ScanQRFragment(), "SCAN QR");
        viewPager.setAdapter(adapter);
    }

    public void goToMainScreen(){
        Intent intent = new Intent(LoginActivity.this , MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }
}
