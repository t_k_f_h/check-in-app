package checkin.project.com.checkinapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ThanksActivity extends AppCompatActivity {

    private TextView textMessage;
    private Button btnDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanks);

        initUIs();
        initEvents();

        loadData();
    }

    private void loadData() {
        Intent intent = getIntent();
        if(intent != null){
            int subscriptionType = intent.getIntExtra("customer_subscription_type", 1);
            if(subscriptionType == 1){
                textMessage.setText("Thank you for Check In");
            }else {
                textMessage.setText(" Thank you for subscribing.");
            }
        }
    }

    private void initUIs() {
        textMessage = (TextView) findViewById(R.id.text_message);
        btnDone = (Button) findViewById(R.id.btn_done);
    }

    private void initEvents() {
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ThanksActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
    }

}
