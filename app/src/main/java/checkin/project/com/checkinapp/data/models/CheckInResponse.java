package checkin.project.com.checkinapp.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by khacvy on 4/10/17.
 */

public class CheckInResponse {
    private CustomerInfo customer;
    @SerializedName("check-in")
    private CheckInInfo checkInInfo;
    private String status;

    public CustomerInfo getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerInfo customer) {
        this.customer = customer;
    }

    public CheckInInfo getCheckInInfo() {
        return checkInInfo;
    }

    public void setCheckInInfo(CheckInInfo checkInInfo) {
        this.checkInInfo = checkInInfo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
