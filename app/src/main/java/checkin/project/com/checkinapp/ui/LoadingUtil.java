package checkin.project.com.checkinapp.ui;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.kaopiz.kprogresshud.KProgressHUD;

import checkin.project.com.checkinapp.R;

/**
 * Created by khacvy on 4/9/17.
 */

public class LoadingUtil {

    private static KProgressHUD mKProgressHUB;

    private LoadingUtil() {
    }

    public static void showLoading(Context context){
        mKProgressHUB = KProgressHUD.create(context)
                .setWindowColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(context.getString(R.string.common_label_loading))
                .setDimAmount(0.5f).show();
    }

    public static void hideLoading(){
        if(mKProgressHUB != null && mKProgressHUB.isShowing()){
            mKProgressHUB.dismiss();
        }
    }

}
