package checkin.project.com.checkinapp.data;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import checkin.project.com.checkinapp.App;
import checkin.project.com.checkinapp.data.callbacks.BaseCallback;
import checkin.project.com.checkinapp.data.callbacks.LoginCallback;
import checkin.project.com.checkinapp.data.models.BaseResponse;
import checkin.project.com.checkinapp.data.models.CheckInResponse;
import checkin.project.com.checkinapp.data.models.CustomerInfo;
import checkin.project.com.checkinapp.data.models.LoginResponse;
import checkin.project.com.checkinapp.data.models.UserInfoResponse;

/**
 * Created by khacvy on 4/9/17.
 */

public class AppApi {
    private static final String TAG = "AppApi";

    private static final String BASE_URL = "http://bas.dtsmart.net/api/v1/";
    private static final String LOGIN_API_URL = BASE_URL + "user/login";
    private static final String USER_INFO_API_URL = BASE_URL + "user/info";
    private static final String CHECK_IN_PHONE_API_URL = BASE_URL + "customer/check-in";
    private static final String UPDATE_CUSTOMER_INFO_API_URL = BASE_URL + "customer/update";
    private static final String CHECK_IN_SUBMIT_API_URL = BASE_URL + "customer/check-in-submit";

    private static final String STATUS_OK = "ok";
    private static final String STATUS_FAIL = "fail";

    public void authenticate(String username, String password, final LoginCallback
            callback) {
        final Map<String, String> requestBody = new HashMap<>();
        requestBody.put("username", username);
        requestBody.put("password", password);

        Type type = new TypeToken<BaseResponse<LoginResponse>>() {
        }.getType();

        BaseRequest<BaseResponse<LoginResponse>> request = new BaseRequest<>(
                LOGIN_API_URL,
                type,
                null,
                requestBody,
                new Response.Listener<BaseResponse<LoginResponse>>() {
                    @Override
                    public void onResponse(BaseResponse<LoginResponse> response) {
                        if (response.getResult().getStatus().equalsIgnoreCase(STATUS_OK)) {
                            callback.onSuccess(response.getResult());
                        } else {
                            callback.onLoginFailed();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: ", error);
                        callback.onError(error.getMessage());
                    }
                }
        );

        App.getInstance().addToRequestQueue(request);
    }

    public void getUserInfo(Context context, final BaseCallback<UserInfoResponse>
            callback) {
        Type type = new TypeToken<BaseResponse<UserInfoResponse>>() {
        }.getType();
        BaseRequest<BaseResponse<UserInfoResponse>> request = new BaseRequest<>(
                context,
                USER_INFO_API_URL,
                Request.Method.GET,
                type,
                null,
                null,
                new Response.Listener<BaseResponse<UserInfoResponse>>() {
                    @Override
                    public void onResponse(BaseResponse<UserInfoResponse> response) {
                        if (response.getStatus().equalsIgnoreCase(STATUS_OK)) {
                            callback.onSuccess(response.getResult());
                        } else {
                            callback.onError(null);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: ", error);
                        callback.onError(error.getMessage());
                    }
                }
        );

        App.getInstance().addToRequestQueue(request);
    }

    public void checkIn(Context context, String phone, final BaseCallback<CheckInResponse>
            callback) {

        String url = "";
        try {
            url = CHECK_IN_PHONE_API_URL + "?phone=" + URLEncoder.encode(phone, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "checkIn: ", e);
        }
        Type type = new TypeToken<BaseResponse<CheckInResponse>>() {
        }.getType();
        BaseRequest<BaseResponse<CheckInResponse>> request = new BaseRequest<BaseResponse<CheckInResponse>>(
                context,
                url,
                Request.Method.GET,
                type,
                null,
                null,
                new Response.Listener<BaseResponse<CheckInResponse>>() {
                    @Override
                    public void onResponse(BaseResponse<CheckInResponse> response) {
                        if (response.getStatus().equalsIgnoreCase(STATUS_OK)) {
                            callback.onSuccess(response.getResult());
                        } else {
                            callback.onError(null);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: ", error);
                        callback.onError(error.getMessage());
                    }
                }
        );

        App.getInstance().addToRequestQueue(request);
    }

    public void updateCustomerInfo(Context context, CustomerInfo info, final
    BaseCallback<CustomerInfo>
            callback) {
        Map<String, String> params = new HashMap<>();
        params.put("Customer[first_name]", info.getFirstName());
        params.put("Customer[last_name]", info.getLastName());
        params.put("Customer[birthday]", info.getBirthday());
        params.put("Customer[email]", info.getEmail());
        params.put("phone", info.getPhone());

        Type type = new TypeToken<BaseResponse<CustomerInfo>>() {
        }.getType();
        BaseRequest<BaseResponse<CustomerInfo>> request = new BaseRequest<>(
                context,
                UPDATE_CUSTOMER_INFO_API_URL,
                Request.Method.POST,
                type,
                params,
                null,
                new Response.Listener<BaseResponse<CustomerInfo>>() {
                    @Override
                    public void onResponse(BaseResponse<CustomerInfo> response) {
                        if (response.getStatus().equalsIgnoreCase(STATUS_OK)) {
                            callback.onSuccess(response.getResult());
                        } else if(response.getStatus().equalsIgnoreCase(STATUS_FAIL)) {
                            callback.onError(response.getResult().getErrors());
                        }else {
                            callback.onError(null);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: ", error);
                        callback.onError(error.getMessage());
                    }
                }
        );

        App.getInstance().addToRequestQueue(request);
    }

    public void checkInSubmit(Context context, String phone, final BaseCallback<CheckInResponse>
            callback) {

        String url = "";
        try {
            url = CHECK_IN_SUBMIT_API_URL + "?phone=" + URLEncoder.encode(phone, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "checkIn: ", e);
        }
        Type type = new TypeToken<BaseResponse<CheckInResponse>>() {
        }.getType();
        BaseRequest<BaseResponse<CheckInResponse>> request = new BaseRequest<BaseResponse<CheckInResponse>>(
                context,
                url,
                Request.Method.GET,
                type,
                null,
                null,
                new Response.Listener<BaseResponse<CheckInResponse>>() {
                    @Override
                    public void onResponse(BaseResponse<CheckInResponse> response) {
                        if (response.getStatus().equalsIgnoreCase(STATUS_OK)) {
                            callback.onSuccess(response.getResult());
                        } else {
                            callback.onError(null);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: ", error);
                        callback.onError(error.getMessage());
                    }
                }
        );

        App.getInstance().addToRequestQueue(request);
    }
}
