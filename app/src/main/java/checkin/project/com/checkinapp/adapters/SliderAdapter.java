package checkin.project.com.checkinapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import checkin.project.com.checkinapp.R;
import checkin.project.com.checkinapp.data.models.DealInfo;

/**
 * Created by khacvy on 4/10/17.
 */

public class SliderAdapter extends BaseAdapter {

    private List<DealInfo> mDealInfoList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public SliderAdapter(List<DealInfo> dealInfoList, Context context) {
        mDealInfoList = dealInfoList;
        mContext = context;
        mLayoutInflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return mDealInfoList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDealInfoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.slider_view_item, null);

        TextView textDescription = (TextView) convertView.findViewById(R.id.text_description);
        ImageView imgLogo = (ImageView) convertView.findViewById(R.id.img_logo);

        DealInfo dealInfo = mDealInfoList.get(position);
        if(dealInfo.getType().equalsIgnoreCase("text")){
            textDescription.setText(dealInfo.getContent());
        }else if(dealInfo.getType().equalsIgnoreCase("image")){
            textDescription.setText(dealInfo.getContent());

            Glide.with(mContext).load(dealInfo.getUrl())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgLogo);
        }

        return convertView;
    }
}
