package checkin.project.com.checkinapp.data.callbacks;

/**
 * Created by khacvy on 4/9/17.
 */

public interface BaseCallback<T> {

    void onSuccess(T t);

    void onError(String error);
}
