package checkin.project.com.checkinapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterViewFlipper;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import checkin.project.com.checkinapp.adapters.SliderAdapter;
import checkin.project.com.checkinapp.data.AppApi;
import checkin.project.com.checkinapp.data.PreferenceManager;
import checkin.project.com.checkinapp.data.callbacks.BaseCallback;
import checkin.project.com.checkinapp.data.models.CheckInResponse;
import checkin.project.com.checkinapp.data.models.CustomerInfo;
import checkin.project.com.checkinapp.data.models.UserInfoResponse;
import checkin.project.com.checkinapp.ui.LoadingUtil;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";

    private EditText txtPhoneNum;
    private Button btnNum1;
    private Button btnNum2;
    private Button btnNum3;
    private Button btnNum4;
    private Button btnNum5;
    private Button btnNum6;
    private Button btnNum7;
    private Button btnNum8;
    private Button btnNum9;
    private Button btnNum0;
    private ImageView btnRemove;
    private Button btnSubmit;
    private AdapterViewFlipper mViewFlipper;

    private AppApi mAppApi;
    private PreferenceManager mPreferenceManager;

    private static final String CHECK_IN_STATUS_PEDDING = "CHECK_IN_PEDDING";
    private static final String CHECK_IN_STATUS_ACTIVE = "CHECK_IN_ACTIVE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAppApi = new AppApi();
        mPreferenceManager = new PreferenceManager(this);

        initUIs();

        initEvents();

        loadBusinessInfo();
    }

    private void initEvents() {
        btnNum0.setOnClickListener(this);
        btnNum1.setOnClickListener(this);
        btnNum2.setOnClickListener(this);
        btnNum3.setOnClickListener(this);
        btnNum4.setOnClickListener(this);
        btnNum5.setOnClickListener(this);
        btnNum6.setOnClickListener(this);
        btnNum7.setOnClickListener(this);
        btnNum8.setOnClickListener(this);
        btnNum9.setOnClickListener(this);
        btnRemove.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        WeakReference<EditText> weakReference = new WeakReference<EditText>(txtPhoneNum);
        txtPhoneNum.addTextChangedListener(new CustomTextWatcher(weakReference));

    }

    private void initUIs() {
        txtPhoneNum = (EditText) findViewById(R.id.txt_phone_num);
        btnNum0 = (Button) findViewById(R.id.btnNum0);
        btnNum1 = (Button) findViewById(R.id.btnNum1);
        btnNum2 = (Button) findViewById(R.id.btnNum2);
        btnNum3 = (Button) findViewById(R.id.btnNum3);
        btnNum4 = (Button) findViewById(R.id.btnNum4);
        btnNum5 = (Button) findViewById(R.id.btnNum5);
        btnNum6 = (Button) findViewById(R.id.btnNum6);
        btnNum7 = (Button) findViewById(R.id.btnNum7);
        btnNum8 = (Button) findViewById(R.id.btnNum8);
        btnNum9 = (Button) findViewById(R.id.btnNum9);
        btnRemove = (ImageView) findViewById(R.id.btn_remove);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        mViewFlipper = (AdapterViewFlipper) findViewById(R.id.view_flipper);

    }

    private void loadBusinessInfo(){
        LoadingUtil.showLoading(this);
        mAppApi.getUserInfo(this, new BaseCallback<UserInfoResponse>() {
            @Override
            public void onSuccess(UserInfoResponse userInfoResponse) {
                LoadingUtil.hideLoading();
                mPreferenceManager.saveBusinessInfo(userInfoResponse.getBusiness());
                setTitle(userInfoResponse.getBusiness().getName());

                SliderAdapter adapter = new SliderAdapter(userInfoResponse.getDeals(),
                        getBaseContext());
                mViewFlipper.setAdapter(adapter);
                mViewFlipper.startFlipping();
            }

            @Override
            public void onError(String error) {
                LoadingUtil.hideLoading();
                String errorMsg = (error != null) ? error : getString(R.string.common_server_error);
                Toast.makeText(getBaseContext(), errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnNum0:
                pressNumber(0);
                break;
            case R.id.btnNum1:
                pressNumber(1);
                break;
            case R.id.btnNum2:
                pressNumber(2);
                break;
            case R.id.btnNum3:
                pressNumber(3);
                break;
            case R.id.btnNum4:
                pressNumber(4);
                break;
            case R.id.btnNum5:
                pressNumber(5);
                break;
            case R.id.btnNum6:
                pressNumber(6);
                break;
            case R.id.btnNum7:
                pressNumber(7);
                break;
            case R.id.btnNum8:
                pressNumber(8);
                break;
            case R.id.btnNum9:
                pressNumber(9);
                break;
            case R.id.btn_remove:
                clearText();
                break;
            case R.id.btnSubmit:
                processSubmit();
                break;
        }
    }

    private void processSubmit() {
        final String phoneNum = txtPhoneNum.getText().toString().trim().replaceAll("\\W", "");
        if (validate(phoneNum)) {
            LoadingUtil.showLoading(this);
            mAppApi.checkIn(this, phoneNum, new BaseCallback<CheckInResponse>() {
                @Override
                public void onSuccess(CheckInResponse response) {
                    String status = response.getStatus();
                    if (status.equalsIgnoreCase(CHECK_IN_STATUS_ACTIVE)) {
                        submitCheckIn(phoneNum);
                    } else {
                        LoadingUtil.hideLoading();
                        if (!checkCustomerInfo(response.getCustomer())) {
                            Intent intent = new Intent(MainActivity.this, UserInfoActivity.class);
                            intent.putExtra("CUSTOMER_INFO", response.getCustomer());
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                }

                @Override
                public void onError(String error) {
                    LoadingUtil.hideLoading();
                    String errorMsg = (error != null) ? error : getString(R.string.common_server_error);
                    Toast.makeText(getBaseContext(), errorMsg, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void clearText() {
        String text = txtPhoneNum.getText().toString().trim();
        text = text.replaceAll("\\W", "");
        if (!TextUtils.isEmpty(text)) {
            text = text.substring(0, text.length() - 1);
            txtPhoneNum.setText(text);
        }
    }

    private void pressNumber(int number) {
        String text = txtPhoneNum.getText().toString().trim();
        txtPhoneNum.setText(text + number);
    }

    private boolean validate(String phoneNum) {
        if (!Patterns.PHONE.matcher(phoneNum).matches() || phoneNum.length() < 9) {
            txtPhoneNum.setError("Please enter a valid phone number!");
            return false;
        }
        return true;
    }

    private boolean checkCustomerInfo(CustomerInfo info) {
        if(TextUtils.isEmpty(info.getFirstName())
                || TextUtils.isEmpty(info.getLastName())
                ||TextUtils.isEmpty(info.getEmail())
                ||TextUtils.isEmpty(info.getBirthday())){
            return false;
        }
        return true;
    }

    private void submitCheckIn(String phone){
        mAppApi.checkInSubmit(this, phone, new BaseCallback<CheckInResponse>() {
            @Override
            public void onSuccess(CheckInResponse response) {
                LoadingUtil.hideLoading();

                Intent intent = new Intent(MainActivity.this, ThanksActivity.class);
                intent.putExtra("customer_subscription_type", 1);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }

            @Override
            public void onError(String error) {
                LoadingUtil.hideLoading();
                String errorMsg = (error != null) ? error : getString(R.string.common_server_error);
                Toast.makeText(getBaseContext(), errorMsg, Toast.LENGTH_LONG).show();
            }
        });
    }

    class CustomTextWatcher implements TextWatcher {
        //This TextWatcher sub-class formats entered numbers as 1 (123) 456-7890
        private boolean mFormatting; // this is a flag which prevents the
        // stack(onTextChanged)
        private boolean clearFlag;
        private int mLastStartLocation;
        private String mLastBeforeText;
        private WeakReference<EditText> mWeakEditText;

        public CustomTextWatcher(WeakReference<EditText> weakEditText) {
            this.mWeakEditText = weakEditText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            if (after == 0 && s.toString().equals("1 ")) {
                clearFlag = true;
            }
            mLastStartLocation = start;
            mLastBeforeText = s.toString();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // TODO: Do nothing
        }

        @Override
        public void afterTextChanged(Editable s) {
            String currentValue = s.toString();
            //hide clear button
            if (TextUtils.isEmpty(currentValue)) {
                btnRemove.setVisibility(View.GONE);
            } else {
                btnRemove.setVisibility(View.VISIBLE);
            }
            //remove error
            txtPhoneNum.setError(null);
            //show/hide submit
            String phoneNum = currentValue.trim().replaceAll("\\W", "");
            if(phoneNum.startsWith("1")){
                if(phoneNum.length() >= 9){
                    btnSubmit.setVisibility(View.VISIBLE);
                }else {
                    btnSubmit.setVisibility(View.GONE);
                }
            }else {
                if(phoneNum.length() >= 10){
                    btnSubmit.setVisibility(View.VISIBLE);
                }else {
                    btnSubmit.setVisibility(View.GONE);
                }
            }

            // Make sure to ignore calls to afterTextChanged caused by the work
            // done below
            if (!mFormatting) {
                mFormatting = true;
                int curPos = mLastStartLocation;
                String beforeValue = mLastBeforeText;

                String formattedValue = formatUsNumber(s);
                if (currentValue.length() > beforeValue.length()) {
                    int setCusorPos = formattedValue.length()
                            - (beforeValue.length() - curPos);
                    mWeakEditText.get().setSelection(setCusorPos < 0 ? 0 : setCusorPos);
                } else {
                    int setCusorPos = formattedValue.length()
                            - (currentValue.length() - curPos);
                    if (setCusorPos > 0 && !Character.isDigit(formattedValue.charAt(setCusorPos - 1))) {
                        setCusorPos--;
                    }
                    mWeakEditText.get().setSelection(setCusorPos < 0 ? 0 : setCusorPos);
                }
                mFormatting = false;
            }
        }

        private String formatUsNumber(Editable text) {
            StringBuilder formattedString = new StringBuilder();
            // Remove everything except digits
            int p = 0;
            while (p < text.length()) {
                char ch = text.charAt(p);
                if (!Character.isDigit(ch)) {
                    text.delete(p, p + 1);
                } else {
                    p++;
                }
            }
            // Now only digits are remaining
            String allDigitString = text.toString();

            int totalDigitCount = allDigitString.length();

            if (totalDigitCount == 0
                    || (totalDigitCount > 10 && !allDigitString.startsWith("1"))
                    || totalDigitCount > 11) {
                // May be the total length of input length is greater than the
                // expected value so we'll remove all formatting
                text.clear();
                text.append(allDigitString);
                return allDigitString;
            }
            int alreadyPlacedDigitCount = 0;
            // Only '1' is remaining and user pressed backspace and so we clear
            // the edit text.
            if (allDigitString.equals("1") && clearFlag) {
                text.clear();
                clearFlag = false;
                return "";
            }
            if (allDigitString.startsWith("1")) {
                formattedString.append("1 ");
                alreadyPlacedDigitCount++;
            }
            // The first 3 numbers beyond '1' must be enclosed in brackets "()"
            if (totalDigitCount - alreadyPlacedDigitCount > 3) {
                formattedString.append("("
                        + allDigitString.substring(alreadyPlacedDigitCount,
                        alreadyPlacedDigitCount + 3) + ") ");
                alreadyPlacedDigitCount += 3;
            }
            // There must be a '-' inserted after the next 3 numbers
            if (totalDigitCount - alreadyPlacedDigitCount > 3) {
                formattedString.append(allDigitString.substring(
                        alreadyPlacedDigitCount, alreadyPlacedDigitCount + 3)
                        + "-");
                alreadyPlacedDigitCount += 3;
            }
            // All the required formatting is done so we'll just copy the
            // remaining digits.
            if (totalDigitCount > alreadyPlacedDigitCount) {
                formattedString.append(allDigitString
                        .substring(alreadyPlacedDigitCount));
            }

            text.clear();
            text.append(formattedString.toString());
            return formattedString.toString();
        }
    }
}
