package checkin.project.com.checkinapp.data.models;

/**
 * Created by khacvy on 4/9/17.
 */

public class BaseResponse<T> {
    private String status;
    private T result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
