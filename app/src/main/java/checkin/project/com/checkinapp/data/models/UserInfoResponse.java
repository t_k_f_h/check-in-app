package checkin.project.com.checkinapp.data.models;

import java.util.List;

/**
 * Created by khacvy on 4/9/17.
 */

public class UserInfoResponse {
    private UserInfo info;
    private BusinessInfo business;
    private List<DealInfo> deals;

    public UserInfo getInfo() {
        return info;
    }

    public void setInfo(UserInfo info) {
        this.info = info;
    }

    public BusinessInfo getBusiness() {
        return business;
    }

    public void setBusiness(BusinessInfo business) {
        this.business = business;
    }

    public List<DealInfo> getDeals() {
        return deals;
    }

    public void setDeals(List<DealInfo> deals) {
        this.deals = deals;
    }
}
